---
document: modulemd
version: 2
data:
  name: mongodb
  stream: 3.6
  version: 8010020191120190538
  context: 1cd76107
  summary: MongoDB Module
  description: >-
    Mongo from humongous is a high-performance, open source, schema-free document-oriented
    database. MongoDB is written in C++ and offers the following features. Collection
    oriented storage, easy storage of object/JSON-style data. Dynamic queries. Full
    index support, including on inner objects and embedded arrays. Query profiling.
    Replication and fail-over support. Efficient storage of binary data including
    large objects (e.g. photos and videos). Auto-sharding for cloud-level scalability
    (currently in early alpha). Commercial Support Available. A key goal of MongoDB
    is to bridge the gap between key/value stores (which are fast and highly scalable)
    and traditional RDBMS systems (which are deep in functionality).
  license:
    module:
    - MIT
  xmd:
    mbs:
      mse: TRUE
      buildrequires:
        javapackages-tools:
          ref: b54716a3682105c0f4c301913701be086c0cdfcc
          stream: 201801
          context: b07bea58
          version: 8000020190628172923
          koji_tag: module-javapackages-tools-201801-8000020190628172923-b07bea58
          filtered_rpms: []
        go-toolset:
          ref: a32c5fe0a1c10fa78772b637d3f294563f6ced7d
          stream: rhel8
          context: ccff3eb7
          version: 8010020191119053516
          koji_tag: module-go-toolset-rhel8-8010020191119053516-ccff3eb7
          filtered_rpms: []
        platform:
          ref: virtual
          stream: el8.1.0
          context: 00000000
          version: 2
          koji_tag: module-centos-8.1.0-build
          stream_collision_modules: 
          ursine_rpms: 
          filtered_rpms: []
      scmurl: git+https://git.centos.org/modules/mongodb.git?#5454a7f6041f5446c59093ef2749f3a9c0ef6ff8
      commit: 5454a7f6041f5446c59093ef2749f3a9c0ef6ff8
      rpms:
        jctools:
          ref: 050d58f8ee897d0d0de3e2f4993c9a8166f46add
        python-pymongo:
          ref: c7b6fe46e6e0b53984d9e7c92765ceac2311e041
        mongodb:
          ref: ad0b723430751f37f7127a80f91de361b560fecf
        slf4j:
          ref: 776780893bfdf66deec2f1c682cd571ec53f7b3d
        netty:
          ref: c11c1a34cfc9302e48d2e060a71ce1dddc6b706b
        python-cheetah:
          ref: b7a7dcc8dedb8f7b8bfee42ca1f8aaf4cfbb1685
        mongo-java-driver:
          ref: af8ad672f5c435c1447df0ee2afc69c87e2c6676
        snappy-java:
          ref: 6ab407acf5592f1aabcfb47bb2b5b467baa04fd6
        mongo-tools:
          ref: e7a6fa34fbdf53b08f86fa1455a70b444f6f29d4
        yaml-cpp:
          ref: 3d9bc35b146d1d27a887a625b67a1a7d5bd511c7
  dependencies:
  - buildrequires:
      go-toolset: [rhel8]
      javapackages-tools: [201801]
      platform: [el8.1.0]
    requires:
      platform: [el8]
  references:
    community: https://docs.pagure.org/modularity/
    documentation: https://github.com/container-images/mongodb/
    tracker: https://github.com/modularity-modules/mongodb
  profiles:
    client:
      rpms:
      - mongo-tools
      - mongodb
    default:
      rpms:
      - mongodb
      - mongodb-server
    server:
      rpms:
      - mongodb-server
  api:
    rpms:
    - mongo-tools
    - mongodb
    - mongodb-server
  filter:
    rpms:
    - jcl-over-slf4j
    - jctools-experimental
    - jctools-javadoc
    - jctools-parent
    - jul-to-slf4j
    - log4j-over-slf4j
    - mongo-tools-devel
    - mongodb-test
    - netty-javadoc
    - python-pymongo-doc
    - python3-bson
    - python3-cheetah
    - python3-pymongo
    - python3-pymongo-gridfs
    - slf4j-ext
    - slf4j-javadoc
    - slf4j-jcl
    - slf4j-jdk14
    - slf4j-log4j12
    - slf4j-manual
    - slf4j-sources
    - snappy-java-javadoc
    - yaml-cpp-devel
    - yaml-cpp-static
  buildopts:
    rpms:
      macros: |
        %_with_xmvn_javadoc 1
        %_with_jp_minimal 1
        %_without_tests 1
        %_without_python2 1
  components:
    rpms:
      jctools:
        rationale: Runtime dependency of netty.
        repository: git+https://git.centos.org/rpms/jctools
        cache: https://git.centos.org/repo/pkgs/jctools
        ref: 050d58f8ee897d0d0de3e2f4993c9a8166f46add
        arches: [aarch64, i686, ppc64le, x86_64]
      mongo-java-driver:
        rationale: Java driver for MongoDB
        repository: git+https://git.centos.org/rpms/mongo-java-driver
        cache: https://git.centos.org/repo/pkgs/mongo-java-driver
        ref: af8ad672f5c435c1447df0ee2afc69c87e2c6676
        buildorder: 2
        arches: [aarch64, i686, ppc64le, x86_64]
      mongo-tools:
        rationale: MongoDB tools provides import, export, and diagnostic capabilities.
        repository: git+https://git.centos.org/rpms/mongo-tools
        cache: https://git.centos.org/repo/pkgs/mongo-tools
        ref: e7a6fa34fbdf53b08f86fa1455a70b444f6f29d4
        arches: [aarch64, i686, ppc64le, x86_64]
      mongodb:
        rationale: MongoDB package.
        repository: git+https://git.centos.org/rpms/mongodb
        cache: https://git.centos.org/repo/pkgs/mongodb
        ref: ad0b723430751f37f7127a80f91de361b560fecf
        buildorder: 2
        arches: [aarch64, i686, ppc64le, x86_64]
      netty:
        rationale: Runtime dependency of mongo-java-driver.
        repository: git+https://git.centos.org/rpms/netty
        cache: https://git.centos.org/repo/pkgs/netty
        ref: c11c1a34cfc9302e48d2e060a71ce1dddc6b706b
        buildorder: 1
        arches: [aarch64, i686, ppc64le, x86_64]
      python-cheetah:
        rationale: MongoDB build-only dependecy.
        repository: git+https://git.centos.org/rpms/python-cheetah
        cache: https://git.centos.org/repo/pkgs/python-cheetah
        ref: b7a7dcc8dedb8f7b8bfee42ca1f8aaf4cfbb1685
        arches: [aarch64, i686, ppc64le, x86_64]
      python-pymongo:
        rationale: MongoDB dependecy for running testsuite.
        repository: git+https://git.centos.org/rpms/python-pymongo
        cache: https://git.centos.org/repo/pkgs/python-pymongo
        ref: c7b6fe46e6e0b53984d9e7c92765ceac2311e041
        arches: [aarch64, i686, ppc64le, x86_64]
      slf4j:
        rationale: Runtime dependency of mongo-java-driver.
        repository: git+https://git.centos.org/rpms/slf4j
        cache: https://git.centos.org/repo/pkgs/slf4j
        ref: 776780893bfdf66deec2f1c682cd571ec53f7b3d
        arches: [aarch64, i686, ppc64le, x86_64]
      snappy-java:
        rationale: Optional runtime dependency of mongo-java-driver.
        repository: git+https://git.centos.org/rpms/snappy-java
        cache: https://git.centos.org/repo/pkgs/snappy-java
        ref: 6ab407acf5592f1aabcfb47bb2b5b467baa04fd6
        arches: [aarch64, i686, ppc64le, x86_64]
      yaml-cpp:
        rationale: MongoDB dependency.
        repository: git+https://git.centos.org/rpms/yaml-cpp
        cache: https://git.centos.org/repo/pkgs/yaml-cpp
        ref: 3d9bc35b146d1d27a887a625b67a1a7d5bd511c7
        arches: [aarch64, i686, ppc64le, x86_64]
...
